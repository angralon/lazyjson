/* The user copies the php files to his server and edits the database connection data: */
$connect = array 
(
	'host' => 'localhost',
	'login' => 'root',
	'password' => '1234',
	'database' => 'test' 
);

/* He declares a new LazyJSON object and sets the path to the php script in his android app */
LazyJSON lazy = new LazyJSON("http://www.example.com/lazyJSON/index.php");

/* He does his query: */
result = lazy.query(" SELECT id,name,age FROM users WHERE id >= '11' ");
System.out.println(result.toString());
/* expected output is a  Object with Error statements and a JSONarray: 

	error:false
	errorMsg:null
	JSONArray:
		id:11 name:Urs age:33
		id:22 name:Hans age:43
		id:13 name:Peter age:51

expected error output:

	error:true
	errorMsg:MYSQL error - connection failed
	JSONArray: null

*/

/* Or he uses insert: */
insertedId = lazy.insert (" INSERT INTO users (name,age) VALUES ('Hansli', '64') ");
System.out.println(insertedId.toString());
/* expected output is:

	error:false
	errorMsg:null
	JSONArray: insertedId:23

expected error output:

	error:true
	errorMsg:MYSQL error - connection failed
	JSONArray: null
*/
