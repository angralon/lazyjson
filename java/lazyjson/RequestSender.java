package lazyjson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.HttpURLConnection;
import java.nio.charset.Charset;

import ch.mobile.lazyjson.lazyjsonmobile.lazyjsonlib.JSONArray;
import ch.mobile.lazyjson.lazyjsonmobile.lazyjsonlib.JSONObject;
import ch.mobile.lazyjson.lazyjsonmobile.lazyjsonlib.JSONException;

public class RequestSender 
{
	// local variables
	private static Charset CHARSET = Charset.forName("UTF8");

	private static JSONObject jsonObject;
	private static boolean hasInsertedId = false;
	private static Cryptor cryptor = new Cryptor();

	/**
	 * Connect to the given server and call the php - file <br/>
	 * returns the data which are send back from the server, in the Options - object <br/>
	 * @param options
	 * @throws LazyException 
	 */
	public static void requestSend(Options options)
	{
		//Add elements for the php		
		addOptions(options);

		hasInsertedId = options.hasInsertId();

		readReturnValues(options.getLazyAnswer(), getDatabaseData(options.getLazyAnswer(), options.getScriptLocation()));
	}


	//Private Methodes	

	/**
	 * Add the database options for the php - file to a nameValuePair
	 */
	private static void addOptions(Options options)
	{
		jsonObject = new JSONObject();

		try 
		{
			jsonObject.put("query", options.getSql());
			jsonObject.put("usesInsertId", Boolean.toString(options.hasInsertId()));
		}
		catch (JSONException e) 
		{
			options.getLazyAnswer().setError(true);
			options.getLazyAnswer().setErrorMessage(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Make a connection with the server and submit the json object, if the server returns a result, it's saved in an string object,  <br/>
	 * if there is no result, there is an empty string returned <br/>
	 * @param lazyAnswer <br/>
	 * @param scriptLocation <br/>
	 * @return the result from the server as a string object or an empty string, if there is no result from the server or an error
	 * @throws LazyException 
	 */
	private static String getDatabaseData(LazyAnswer lazyAnswer, String scriptLocation)
	{
		InputStream inputStream = null;
		jsonObject = cryptor.jsonObjectEncrypt(jsonObject);

		//http post
		try
		{
			// TODO make a parameter specified encryption (0 -> objects are encrypted, 1 -> string is encrypted)
			byte[] outputParameters = ("jsonpost=" + jsonObject.toString()).getBytes(CHARSET);

			URL url = new URL(scriptLocation);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// add request header
			connection.setRequestMethod("POST");		// use post method
			connection.setChunkedStreamingMode(0);		// use default chunk size

			// prepare outputstream for sending data to the given connection
			connection.setDoOutput(true);
			DataOutputStream dos = new DataOutputStream(connection.getOutputStream());

			// send the object to the given connection
			dos.write(outputParameters);
			dos.flush();
			dos.close();

			// wait for the server answer
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line = "";
			StringBuilder sb = new StringBuilder();

			while((line = br.readLine()) != null)
				sb.append(line);

			return sb.toString();
		}
		catch(Exception e)
		{
			lazyAnswer.setError(true);
			lazyAnswer.setErrorMessage(e.toString());
			lazyAnswer.setNetworkError(true);
		}

		return "";
	}

	/**
	 * Reads the returns from the database in a JSONArray in the lazyAnswer - object <br/>
	 * @param lazyAnswer for the return value <br/>
	 * @param result contains the returns from the database <br/>
	 * @throws LazyException 
	 */
	private static void readReturnValues(LazyAnswer lazyAnswer, String result)
	{
		JSONArray jsonArr = null;		

		if(!lazyAnswer.hasError())
		{
			if(!result.equals("") && result.length()>0 && result.indexOf("[") != -1)
			{
				result = result.substring(result.indexOf("["),result.length());
				jsonArr = new JSONArray(result);
				jsonArr = cryptor.jsonArrayDecrypt(jsonArr);
			}
			else
			{
				lazyAnswer.setError(true);
				lazyAnswer.setErrorMessage("The returned String is empty or doesn't contain a JSONArray");
			}
		}		
		setLazyAnswer(lazyAnswer, jsonArr);
	}

	/**
	 * Split the result from the database into the right result for the user <br />
	 * @param lazyAnswer in this object the answer is inserted <br />
	 * @param jsonArr in this object is the result from the database, as defined <br />
	 * 			{'exceptionFlag' => 'false','exceptionText'	=> '','insertId' => 'no insert id'} <br />
	 * 			{"id":"3","user":"12","article":"gold","quantity":"10"} <br />
	 * 			{"id":"4","user":"13","article":"b3","quantity":"5"} <br />
	 * 			{"id":"5","user":"15","article":"h7","quantity":"6"} <br />
	 * 			...
	 */
	private static void setLazyAnswer(LazyAnswer lazyAnswer, JSONArray jsonArr)
	{
		JSONArray jsonAnswer = new JSONArray();

		try
		{
			// Get the error message
			JSONObject obj = jsonArr.getJSONObject(0);
			lazyAnswer.setError(obj.getBoolean("exceptionFlag"));
			if(lazyAnswer.hasError())
				lazyAnswer.setErrorMessage(obj.getString("exceptionText"));

			// Add the insertedId to the array if the user want it
			if(hasInsertedId)
				jsonAnswer.put(new JSONObject().put("insertedId", String.valueOf(obj.getInt("insertId"))));

			for(int i = 1; i < jsonArr.length(); i++)
			{
				obj = jsonArr.getJSONObject(i);
				jsonAnswer.put(obj);
			}
		}
		catch(JSONException e)
		{
			lazyAnswer.setError(true);
			lazyAnswer.setErrorMessage(e.getMessage());
		}
		catch(NullPointerException e)
		{
			/* Don't do anything, cause JSONObject is empty */
		}
		lazyAnswer.setReturn(jsonAnswer);
	}
}