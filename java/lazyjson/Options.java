package lazyjson;

public class Options 
{
	private String scriptLocation;
	private String sql;
	private boolean insertId;
	private LazyAnswer lazyAnswer;
	
	public Options(LazyAnswer lazyAnswer, String scriptString, boolean insertId, String sql)
	{
		this.lazyAnswer = lazyAnswer;
		this.scriptLocation = scriptString;
		this.insertId = insertId;
		this.sql = sql;
	}

	public String getScriptLocation() 
	{
		return scriptLocation;
	}

	public boolean hasInsertId() 
	{
		return insertId;
	}

	public LazyAnswer getLazyAnswer() 
	{
		return lazyAnswer;
	}
	
	public String getSql()
	{
		return sql;
	}
}
