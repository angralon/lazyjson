package lazyjson;

import ch.mobile.lazyjson.lazyjsonmobile.lazyjsonlib.JSONArray;

public class LazyAnswer
{
	// Local variables
	private boolean hasError;
	private StringBuilder errorMessage;
	private JSONArray returnValues;
	private boolean networkError = false;
	
	/**
	 * Constructor <br/>
	 * Define all variables
	 */
	public LazyAnswer() 
	{
		hasError = false;
		errorMessage = new StringBuilder();
		returnValues = new JSONArray();
	}

	/**
	 * If the query throws an error you get as return value true <br/>
	 * @return hasError
	 */
	public boolean hasError()
	{
		return hasError;
	}
	
	/**
	 * Set the variable hasError, if your query throws an error <br/>
	 * @param has
	 */
	public void setError(boolean has)
	{
		hasError = has;
	}
	
	/**
	 * If the query throws an error message you'll get the error message her <br/>
	 * @return errrorMessage
	 */
	public String getErrorMessage()
	{
		return errorMessage.toString();
	}
	
	/**
	 * If there is an error, it will fill the errorMessage variable <br/>
	 * @param message
	 */
	public void setErrorMessage(String message)
	{
		errorMessage.append("\n");
		errorMessage.append(message);
	}
	
	/**
	 * If the query did not throw an error you get your result from the database <br/>
	 * Test first the method hasError, because the method returns a null value if the query fails or the query returns no values <br/>
	 * @return returnValues
	 */
	public JSONArray getReturn()
	{
		return returnValues;
	}
	
	/**
	 * Fill the return variable with the database <br/>
	 * @param jsonArray
	 */
	public void setReturn(JSONArray jsonArray)
	{
		returnValues = jsonArray;
	}

	public boolean hasNetworkError() {
		return networkError;
	}

	public void setNetworkError(boolean networkError) {
		this.networkError = networkError;
	}
}
