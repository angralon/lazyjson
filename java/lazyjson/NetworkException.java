package lazyjson;

/**
 * Happens when LazyJSON tried to query the server, but the connection timed out
 */
public class NetworkException extends Exception {

	private static final long serialVersionUID = -2931981519411555972L;
	
	public NetworkException() {
		super("Networking time limit reached");
	}

	public NetworkException(String message) {
		super(message);
	}

	public NetworkException(Throwable e) {
		super(e);
	}

}
