package lazyjson;

public class LazyException extends Exception {

	private static final long serialVersionUID = 5657058767978972991L;
	
	public LazyException () {
		super("LazyJSON had a problem");
	}
	
	public LazyException (String message) {
		super(message);
	}
}