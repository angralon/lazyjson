package lazyjson;

public class CryptingException extends RuntimeException {

	private static final long serialVersionUID = 7123322995084333687L;

	public CryptingException() {
		super();
	}
	
	public CryptingException(String message) {
		super(message);
	}
}