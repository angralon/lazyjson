package lazyjson;


import android.content.Context;
import android.content.CursorLoader;

public class LazyJSONwithLoader extends CursorLoader
{
	// local variables
	private LazyAnswer lazyAnswer;
	private String scriptLocation;
	private String queryForRun;
	private int queryTimeLimit = 10000;

	/**
	 * Constructor sets the variables for using this data permanently <br/>
	 * @param scriptLocation is where the php - file is on the server(e.g. http://yourServeraddres.com/(yourFileSystem)/yourPhpFile.php <br/>
	 * @param queryTimeLimit How long LazyJSON should try to query the server before a TimeLimitReachedException is thrown. Set it to 0 to have it use the default value
	 */
	public LazyJSONwithLoader(Context context, String scriptLocation, int queryTimeLimit)
	{
		super(context);
		if(queryTimeLimit != 0)
			this.queryTimeLimit = queryTimeLimit;

		this.scriptLocation = scriptLocation;
	}




	/**
	 * execute the given sql query and return the server result <br/>
	 * @param query for getting some data from the database <br/>
	 * @return the result from the server and if there is an error it returns the error and the error message <br/>
	 * 			error:false <br/>
	 *			errorMsg:null <br/>
	 *			JSONArray: <br/>
	 *				id:11 name:Urs age:33 <br/>
	 *				id:22 name:Hans age:43 <br/>
	 *				id:13 name:Peter age:51 <br/>
	 * <br/>
	 *		expected error output: <br/>
	 *			error:true <br/>
	 *			errorMsg:MYSQL error - connection failed <br/>
	 *			JSONArray: null <br/>
	 * @throws NetworkException is returned if there is no data connection available
	 */
	public synchronized LazyAnswer query(String query) throws NetworkException
	{
		return doCommand(query, false);
	}

	/**
	 * execute the given sql command and return the inserted id back <br/>
	 * @param command for inserting data into the database <br/>
	 * @return the inserted id or if there was an error, it returns the error message <br/>
	 *			error:false <br/>
	 *			errorMsg:null <br/>
	 *			JSONArray: insertedId:23 <br/>
	 * <br/>
	 *		expected error output: <br/>
	 *			error:true <br/>
	 *			errorMsg:MYSQL error - connection failed <br/>
	 *			JSONArray: null <br/>
	 * @throws NetworkException is returned if there is no data connection available
	 */
	public LazyAnswer insert(String command) throws NetworkException
	{
		return doCommand(command, true);
	}
	
	/**
	 * 
	 * @param sqlCommand the command which has to be executed
	 * @param insertedId if true it returns the last inserted id else false, it returns nothing
	 * @return the filled LazyAnswer
	 * @throws NetworkException is returned if there is no data connection available
	 */	
	private synchronized LazyAnswer doCommand(final String sqlCommand, final boolean insertedId) throws NetworkException
	{
		this.lazyAnswer = null;
		this.queryForRun = sqlCommand;
		final Thread mainThread = Thread.currentThread();

		Runnable r = new Runnable()
		{
			@Override
			public void run() 
			{
				if(queryForRun != null) 
				{
					lazyAnswer = new LazyAnswer();
					RequestSender.requestSend(new Options(lazyAnswer, scriptLocation, insertedId, sqlCommand));
					mainThread.interrupt();
				}
			}
		};

		Thread queryThread = new Thread(r);
		queryThread.start();
		synchronized (mainThread)
		{
			try 
			{
				Thread.sleep(queryTimeLimit);
			} 
			catch (InterruptedException e) 
			{
				// do nothing
			}
		}

		if(lazyAnswer != null) 
		{
			if(lazyAnswer.hasNetworkError())
				throw new NetworkException(lazyAnswer.getErrorMessage());

			return lazyAnswer;
		} 
		else 
		{
			throw new NetworkException();
		}
	}
}
