<?php

/**
 * Initialises a Connection with a database via PDO
 * This can be any PDO compatible database like MySQL, PostgreSQL, SQLite, or others
 */
class Connector
 {
	/* Change these values */
	private $dbConfig = array(
		'databaseType' => 'mysql',
		'host' => 'host',
		'user' => 'root',
		'password' => '',
		'database' => 'databaseName', 
		'charset' => 'UTF-8', 		//disable only if you use something else than utf-8
	);
	/* ------------------- */
	
	private $connection;
	
	public function Connector()
	{
		$this->connection = new PDO($this->dbConfig['databaseType'].":host=".$this->dbConfig['host'].";dbname=".$this->dbConfig['database'], $this->dbConfig['user'], $this->dbConfig['password']);
		
		if($this->dbConfig['charset'] == 'UTF-8')
		{
			$this->connection->query("SET NAMES utf8");
		}
		
		$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}	
	
	public function close()
	{
		$this->connection = null;
	}
	
	public function get()
	{
		return $this->connection;
	}
 }
?>