<?php 
/*
	enable these for error handling
	error_reporting(E_ALL);
	ini_set('display_errors', true);
*/
$qH = new QueryHandler();

/**
 * Takes encrypted JSON strings and queries the database that is configured in the Connector.php file with the information within them.
 * Returns the output of the query as encrypted String 
 */
class QueryHandler {

	private $connector,$cryptor;
	private $query;
	private $exceptionText = "";
	private $exceptionFlag = 'false';
	private $usesInsertId = 'false';
	private $result = array();
	private $data;

	public function QueryHandler() {
		
		/* load required files */
		require("./Connector.php");
		require("./Cryptor.php");
		
		/* Init the connection and crypting */
		try 
		{
			$this->connector = new Connector();
			$this->cryptor = new Cryptor();
		}
		catch (PDOException $e)
		{
			$this->exceptionText .= $e->getMessage();
			$this->exceptionFlag = 'true';
		}
			
		/* Get the request information (an encrypted JSON string)*/		
		$json = $_POST['jsonpost'];
		
		
		/* decode the JSON to an array */
		$this->data = json_decode($json, true);	
		/* decrypt the content of the array to another array */					
		$this->data = $this->cryptor->arrayDecrypt($this->data);
		
		
		if($this->data['usesInsertId'] == false || $this->data['usesInsertId'] == 'false') 
		{
			$this->usesInsertId = 'false';
		}
		else 
		{
			$this->usesInsertId = 'true';
		}
		
		/* Do the query */
		$this->query($this->data['query']);
						
		/* encode the array to JSON for returning*/						
		echo json_encode($this->result);		
	}
	
	public function query($query)
	{
		/* open the connection */
		$c = $this->connector->get();
		$insertId = 'no insert id';
		
		try 
		{
   			$res = $c->query($query);
			
			/* store the last inserted id*/
			if($this->usesInsertId == 'true')
			{
				$insertId = $c->lastInsertId();
			}
		} 
		catch(Exception $ex) 
		{
			$this->exceptionText .= $ex->getMessage();
			$this->exceptionFlag = 'true';
		}

		$this->result[] = $this->cryptor->arrayEncrypt(array('exceptionFlag' => $this->exceptionFlag, 'exceptionText' => $this->exceptionText, 'insertId' => $insertId));
		
		if($this->usesInsertId == 'false' && $this->exceptionFlag == 'false')
		{
			while($row = $res->fetch(PDO::FETCH_ASSOC)) 
			{
				$this->result[] = $this->cryptor->arrayEncrypt($row);
			}
		}
		/* Close the connection */
		$this->connector->close();
	}
}
?>
