<?php 

/*
 * Encrypts and decrpyts Strings or Arrays of Strings with Rijndael 128 Bit and CFB
 */
class Cryptor
{
	//Use same as in java Cryptor
	private $iv = '1234567890qwertz';
	private $secretKey = '1234567890qwertz'; 

	function __construct()
	{
	}
	
	/**
	 * Decrypts an Array of Strings and returns it as array again
	 */
	function arrayDecrypt($array)
	{
		$returnArray = array();
		
		foreach($array as $key=>$value)
		{
			$newKey = $this->decrypt($key);
			$returnArray[$newKey] = $this->decrypt($value);
		}		
		
		return $returnArray;
	}
	
	/**
	 * Encrypts an Array of Strings and returns it as array again
	 */
	function arrayEncrypt($array)
	{
		$returnArray = array();
		
		foreach($array as $key=>$value)
		{
			$returnArray[$this->encrypt($key)] = $this->encrypt($value);
		}		
		
		return $returnArray;
	}
	
	/**
	 * Encrypts a Sting and returns it as String again
	 */
	function encrypt($input)
	{
	    return base64_encode(
			mcrypt_encrypt( 
				MCRYPT_RIJNDAEL_128,
				$this->secretKey,
				$input,  
				MCRYPT_MODE_CFB,
				$this->iv
			)
	    );
	}

	/**
	 * Decrypts a Sting and returns it as String again
	 */
	function decrypt($input)
	{
		return mcrypt_decrypt(
			MCRYPT_RIJNDAEL_128, 
			$this->secretKey, 
			base64_decode($input), 
			MCRYPT_MODE_CFB, 
			$this->iv
		);
	}
}
?>
