**Configuration**

First Step for a running LazyJSON is to configure some scripts. For connecting to your database, LazyJSON needs some information about it. You can set your database information in the „Connector.php“ file:
		
```PHP
	'databaseType' => 'mysql', 
	'host' => 'localhost', 
	'user' => 'user', 
	'password' => 'pw', 
	'database' => 'database', 
	'charset' => 'UTF-8', 
```
Often, the only thing you have to change is the „user“, „password“ and „database“ fields.

Next you should change your secret key and initialisation vector. Just open up both Cryptor classes (The Java and the PHP one) and change the following values to something other with 16 chars. Make sure u use the same values in the Java and the PHP Cryptor class.

```Java
	private String secretKey = "1234567890qwertz"; 
	private String iv = "1234567890qwertz";
```

Ther is one more thing you might need. If you don't have the apache http components in your project, grab them at http://hc.apache.org/downloads.cgi and add the jars to your build path.

**How to use**

Now your configuration is working, get ready and try it.

```Java
LazyAnswer answer = lazyJSON.query("SELECT id FROM users"); 
if(!answer.hasError()) { 
	try {
		// Do something with the returned table like
		for(int i=0; i<answer.getReturn().length();i++) {
			ids += answer.getReturn().getJSONObject(i).getString("id")+' ';
		}
	} 
	catch (NetworkException e) { 
		// What to do if a network problem occured
	} 
} 
else { 
	// What to do if the query was bad
}
```

